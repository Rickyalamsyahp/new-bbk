import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {BBK, GlobeTest, background, Logo} from '../../assets';
import {Button, Gap} from '../../components';

const GetStarted = ({navigation}) => {
  return (
    <ImageBackground source={background} style={styles.page} >
      <View>
        <Logo style={{width: 100, height: 100}} />
        <Text style={styles.title}>Scan Barcode Barang</Text>
      </View>
      <View>
        <Button
          title="Get Started"
          onPress={() => navigation.navigate('Register')}
        />
        <Gap height={16} />
        <Button
          type="secondary"
          title="Sign In"
          onPress={() => navigation.navigate('Login')}
        />
      </View>
    </ImageBackground>
  );
};

export default GetStarted;

const styles = StyleSheet.create({
  page: {
    padding: 40,
    justifyContent: 'space-between',
    flex: 1,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 28,
    marginTop: 90,
    color: 'white',
    fontFamily: 'Nunito-SemiBold',
  },
});
