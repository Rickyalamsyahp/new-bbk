import React, { useEffect, useRef, useState } from 'react';
import { ActivityIndicator, FlatList, SafeAreaView, StyleSheet, Text } from 'react-native';
import { Transition, Transitioning } from 'react-native-reanimated';
import { getData } from '../../utils';
import {ListItem} from './listHistory';

const historyReguler = ({navigation}) => {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const transitionRef = useRef();
  const isMounted = useRef(false);
  const transition = <Transition.Change interpolation="easeInOut" />;

  useEffect(() => {
    getdataHistory();
  }, []);

  const getdataHistory = async () => {
    let token = await getData('user');
    try {
      await fetch(
        `https://qrcode.sinovatif.com/service/pengajuan-reguler/scan/all`,
        {
          method: 'GET',
          headers: {
            token: token,
          },
        },
      )
        //Sending the currect offset with get request
        .then(response => response.json())
        .then(responseJson => {
          setData(responseJson.datas.datas);
          setLoading(false);
        });
    } catch (error) {
      console.log(error);
    }
  };
  const onPress = () => {
    transitionRef.current.animateNextTransition();
  };

  const renderItem = ({item}) => {
    return (
      <ListItem
        item={item}
        onPress={onPress}
      />
    );
  };


  return isLoading ? (
    <ActivityIndicator />
  ) : (
      <>
        <Transitioning.View
          ref={transitionRef}
          transition={transition}
          style={{flex: 1}}>
          <SafeAreaView style={{flex: 1, marginTop: 20}}>
            <FlatList
              data={data}
              keyExtractor={(item, index) => {
                return `${item.id}${index}`;
              }}
              renderItem={renderItem}
            />
          </SafeAreaView>
        </Transitioning.View>
      </>
  );
};

export default historyReguler;
const styles = StyleSheet.create({
  page: {
    marginLeft: 40,
    marginRight: 40,
    flex: 1,
    backgroundColor: 'white',
  },
  page2: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20,
    fontFamily: 'Nunito-SemiBold',
    color: '#112340',
    marginTop: 40,
    marginBottom: 40,
  },
  inputsContainer: {
    flex: 1,
    marginBottom: 20,
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  MainContainer:
  {
    flex: 1,
    justifyContent: 'center',
    margin: 5,
    paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
  },
 
  footerStyle:
  {
    padding: 7,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 2,
    borderTopColor: 'white'
  },
 
  TouchableOpacity_style:
  {
    padding: 7,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    borderRadius: 5,
  },
 
  TouchableOpacity_Inside_Text:
  {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18
  },
 
  flatList_items:
  {
    fontSize: 20,
    color: '#000',
    padding: 10
  }
});
