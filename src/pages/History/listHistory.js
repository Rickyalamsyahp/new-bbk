/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import moment from 'moment';

export const ListItem = ({item, onPress, onDelete, ...props}) => {
  const [expanded, setExpanded] = useState(false);

  const onItemPress = () => {
    onPress();
    setExpanded(!expanded);
  };

  return (
    <TouchableOpacity style={styles.wrap} onPress={onItemPress}>
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <View
            style={{
              flex: 1,
              marginTop: 5,
              marginLeft: 7,
            }}>
            <Text style={styles.text}>1. Id Pengujian : {item.idPengajuanreguler}</Text>
          </View>
        </View>
        {expanded && (
          <View style={styles.test}>
            <Text style={styles.text}>
              2. Tanggal Masuk : {item.tanggalmasukPengajuan}
            </Text>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                3. Jam Masuk : {item.waktumasukPengajuan}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                4. Tanggal Keluar : {item.tanggalkeluarPengajuan}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                5. Jam Keluar : {item.waktukeluarPengajuan}
              </Text>
            </View>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrap: {
    borderColor: '#ccc',
    borderWidth: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {width: 10, height: 10},
    shadowOpacity: 0.2,
  },
  container: {flexDirection: 'row', height: 30},
  text: {opacity: 0.7},
  test: {
    marginLeft: 8,
    justifyContent: 'space-around',
    flex: 1,
  },
});
