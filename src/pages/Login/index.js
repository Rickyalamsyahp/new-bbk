import React from 'react';
import {ScrollView, StyleSheet, Text, View, ActivityIndicator} from 'react-native';
import {Logo} from '../../assets';
import {Button, Gap, Input} from '../../components/atoms';
import {useForm, storeData} from '../../utils';

const Login = ({navigation}) => {
  const [form, setForm] = useForm({
    email: '',
    password: '',
  });

  const onContinue = async () => {
    try {
      setForm('reset');
      let res = await fetch('https://qrcode.sinovatif.com/service/login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          emailUser: form.email,
          passwordUser: form.password,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          const a = responseJson;
         
          if (a.status === true) {
            storeData('user', a.token);
            storeData('dataUser', a.datas);
            navigation.replace('MainApp');
          } else {
            alert(a.message);
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ScrollView>
      <View style={styles.page2}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Logo style={{width: 200, height: 200}} />
        </View>
        <View style={styles.page}>
          <Text style={styles.title}>Silahkan Masuk</Text>
          <Input
            label="Email"
            value={form.email}
            onChangeText={value => setForm('email', value)}
          />
          <Gap height={16} />
          <Input
            label="Password"
            secureTextEntry
            value={form.password}
            onChangeText={value => setForm('password', value)}
          />
          <Gap height={40} />
          <Button title="Sign In" onPress={onContinue} />
          <Gap height={20} />
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({
  page: {
    marginLeft: 40,
    marginRight: 40,
    flex: 1,
    backgroundColor: 'white',
  },
  page2: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20,
    fontFamily: 'Nunito-SemiBold',
    color: '#112340',
    marginTop: 40,
    marginBottom: 40,
  },
});
