import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Button, Gap, Header, Input } from '../../components';
import { colors, deletData, getData, storeData } from '../../utils';

const UpdateProfile = ({navigation}) => {
  const [profile, setProfile] = useState({
    namaUser: '',
    emailUser:'',
  });;


  useEffect(() => {
    getUserData()
    // getData('user').then(res => {
    //   const data = res;
    //   data.photoForDB = res?.photo?.length > 1 ? res.photo : ILNullPhoto;
    //   const tempPhoto = res?.photo?.length > 1 ? {uri: res.photo} : ILNullPhoto;
    //   setPhoto(tempPhoto);
    //   setProfile(data);
    // });
  }, []);


  const update = async ()=>{
    const data = {
      namaUser: profile.namaUser,
      emailUser: profile.emailUser
    }

    let token = await getData('user');
    try {
      let res = await fetch(`https://qrcode.sinovatif.com/service/profile/${profile.idUser}/update`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            token: token
          },
          body: JSON.stringify(data),
        })
          .then(response => response.json())
          .then(responseJson => {
            const a = responseJson;
            console.log(a);
            if (a.status === true) {
              storeData('dataUser', a.datas);
              alert(a.message)
            } else {
              console.log(a.message);
              alert(a.message);
            }
          });
     
    } catch (error) {
      
    }
    console.log(data, profile.idUser);
  }
const getUserData = () => {
  getData('dataUser').then(res => {
    setProfile(res);
    console.log(res);
  });
};

const changeText = (key, value) => {
  setProfile({
    ...profile,
    [key]: value,
  });
};
  return (
    <View style={styles.page}>
      <Header title="Edit Profile" onPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.content}>
          <Input
            label="Full Name"
            value={profile.namaUser}
            onChangeText={value => changeText('namaUser', value)}
          />
           <Input
            label="Email"
            value={profile.emailUser}
            onChangeText={value => changeText('emailUser', value)}
          />
          <Gap height={40} />
          <Button title="Save Profile" onPress={update}/>
        </View>
      </ScrollView>
    </View>
  );
};

export default UpdateProfile;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  content: {padding: 40, paddingTop: 0},
});
