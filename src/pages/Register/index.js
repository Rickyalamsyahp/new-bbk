/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {ScrollView} from 'react-native';
import {StyleSheet, View, Text} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {Button, Gap, Input} from '../../components/atoms';
import {Header} from '../../components/molekul';
import {colors, storeData, useForm} from '../../utils';

const Register = ({navigation}) => {
  const [form, setForm] = useForm({
    fullName: '',
    email: '',
    jabatan: '',
    password: '',
    confirmPassword: '',
  });
  const [dataJabatan, setdataJabatan] = useState([]);
  const [selectedValue, setSelectedValue] = useState([]);

  useEffect(async () => {
    getDataJabatan();
  }, []);

  const getDataJabatan = async e => {
    try {
      let res = await fetch('http://qrcode.sinovatif.com/service/role/all', {
        method: 'GET',
      })
        .then(response => response.json())
        .then(data => {
          const dataNow = data.datas;
          var _data = [];
          for (var i = 0; i < dataNow.length; i++) {
            const apj = dataNow[i];
            _data.push({
              idRole: apj.idRole,
              namaRole: apj.namaRole,
            });
          }
          setdataJabatan(_data);
        });
    } catch (error) {
      alert(error);
    }
  };

  const onContinue = async () => {
    try {
      if (form.password === form.confirmPassword) {
        setForm('reset');
        let res = await fetch('https://qrcode.sinovatif.com/service/register', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            namaUser: form.fullName,
            emailUser: form.email,
            idRole: selectedValue,
            passwordUser: form.password,
            passwordKonfirmasi: form.confirmPassword,
          }),
        })
          .then(response => response.json())
          .then(responseJson => {
            const a = responseJson;
            console.log(a);
            if (a.status === true) {
              storeData('user', a.token);
              storeData('dataUser', a.datas);
              navigation.replace('MainApp');
            } else {
              console.log(a.message);
              alert(a.message);
            }
          });
      } else {
        alert('Password dan Password konfirmasi tidak sama');
      }
    } catch (error) {
      alert(error);
    }
  };
  return (
    <ScrollView>
      <View style={styles.page}>
        <Header />
        <View style={styles.contain}>
          <Input
            label="Full Name"
            value={form.fullName}
            onChangeText={value => setForm('fullName', value)}
          />
          <Gap height={24} />
          <Input
            label="Email"
            value={form.email}
            onChangeText={value => setForm('email', value)}
          />
          <Gap height={24} />
          <Text style={styles.label}>Jabatan</Text>
          <Picker
            selectedValue={selectedValue}
            style={{height: 50, width: 150}}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValue(itemValue)
            }>
            {dataJabatan.map(item => {
              return (
                <Picker.Item
                  label={item.namaRole}
                  value={item.idRole}
                  key={item.idRole}
                />
              );
            })}
          </Picker>
          <Gap height={24} />
          <Input
            label="Password"
            secureTextEntry
            value={form.password}
            onChangeText={value => setForm('password', value)}
          />
          <Gap height={24} />
          <Input
            label="Konfirmasi Password"
            secureTextEntry
            value={form.confirmPassword}
            onChangeText={value => setForm('confirmPassword', value)}
          />
          <Gap height={40} />

          <Button title="Continue" onPress={onContinue} />
        </View>
      </View>
    </ScrollView>
  );
};

export default Register;

const styles = StyleSheet.create({
  contain: {
    padding: 40,
    paddingTop: 0,
  },
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  label: {
    fontSize: 16,
    color: colors.text.secondary,
    marginBottom: 6,
  },
});
