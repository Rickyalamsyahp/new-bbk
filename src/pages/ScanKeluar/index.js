import {Card} from 'native-base';
import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Touchable,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {Logo} from '../../assets';
import {Button, HomeProfile} from '../../components';
import {deletData, getData} from '../../utils';
import {Menu, MenuItem, MenuDivider} from 'react-native-material-menu';
import Password from '../UpdatePassword';

const ScanKeluar = ({navigation}) => {
  const scanner = useRef(null);
  const [scan, setScan] = useState(false);
  const [visible, setVisible] = useState(false);
  const [result, setResult] = useState(null);
  const [profile, setProfile] = useState({
    namaUser: '',
    jabatan: '',
  });
  const [scanData, setScanData] = useState();

  useEffect(() => {
    setResult(null);
    getUserData();
    navigation.addListener('focus', () => {
      getUserData();
    });
  }, []);

  const onSuccess = async e => {
    console.log(e);
    if (profile.namaRole === 'Layanan') {
      if (scanData==="reguler") {
        try {
          let token = await getData('user');
          setScan(false);
          let res = await fetch(
            'https://qrcode.sinovatif.com/service/pengajuan-reguler/scan/keluar',
            {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: token,
              },
              body: JSON.stringify({
                idPengajuanreguler: e.data,
              }),
            },
          )
            .then(response => response.json())
            .then(data => alert(data.message));
        } catch (error) {
          alert(error);
      }
    }else{
      try {
        let token = await getData('user');
        setScan(false);
        let res = await fetch(
          'https://qrcode.sinovatif.com/service/pengajuan-sertifikasi/scan/keluar',
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              token: token,
            },
            body: JSON.stringify({
              idPengajuansertifikasi: e.data,
            }),
          },
        )
          .then(response => response.json())
          .then(data => alert(data.message));
      } catch (error) {
        alert(error);
      }
    }
    } else {
      try {
        let token = await getData('user');
        setScan(false);
        let res = await fetch(
          'https://qrcode.sinovatif.com/service/pengajuan-sertifikasi/scan/keluar',
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              token: token,
            },
            body: JSON.stringify({
              idPengajuansertifikasi: e.data,
            }),
          },
        )
          .then(response => response.json())
          .then(data => alert(data.message));
      } catch (error) {
        alert(error);
      }
    }
  };

  const Profile = () => {
    navigation.navigate('Profile');
  };

  const password = () => {
    navigation.navigate('UpdatePassword');
  };
  const hideMenu = () => setVisible(false);
  const showMenu = () => setVisible(true);

  const getUserData = () => {
    getData('dataUser').then(res => {
      setProfile(res);
      console.log(res);
    });
  };

  const logout = () => {
    deletData('user');
    deletData('dataUser');
    navigation.replace('GetStarted');
  };
  return !scan ? (
    <View style={styles.page2}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 10,
          marginRight: 10,
        }}>
        <Card style={{width: 180, height: 50}}>
          <Logo style={{width: 100, height: 50}} />
        </Card>
        <Card style={{width: 180, height: 50}}>
          <Menu
            visible={visible}
            anchor={
              <Text onPress={showMenu}>
                <HomeProfile
                  profile={profile}
                  style={{width: 100, height: 100}}
                />
              </Text>
            }
            onRequestClose={hideMenu}>
            <MenuItem onPress={Profile}>Profile</MenuItem>
            <MenuItem onPress={password}>Edit Password</MenuItem>
            <MenuDivider />
            <MenuItem onPress={logout}>Logout</MenuItem>
          </Menu>
        </Card>
      </View>
      <View style={styles.container}>
        {/* { result && <Text>{JSON.stringify(result, null, 2)}</Text> } */}
        {profile.namaRole === 'Layanan' ? (
          <>
            <Button
              title="Mulai Scan Masuk Pengujian Reguler"
              onPress={() => {
                setScan(true), setScanData('reguler');
              }}
            />
            <View style={{marginTop: 10}}>
              <Button
                title="Mulai Scan Masuk Pengujian Sertifikasi"
                data="sertifikasi"
                onPress={() => {
                  setScan(true), setScanData('sertifikasi');
                }}
              />
            </View>
          </>
        ) : (
          <Button
            title="Mulai Scan Keluar Pengujian Sertifikasi"
            onPress={() => setScan(true)}
          />
        )}
      </View>
    </View>
  ) : (
    <QRCodeScanner
      onRead={onSuccess}
      ref={scanner}
      reactivate={true}
      showMarker={true}
      bottomContent={
        <>
          <TouchableOpacity
            style={styles.buttonTouchable}
            onPress={() => setScan(false)}>
            <Text style={styles.buttonText}>STOP</Text>
          </TouchableOpacity>
        </>
      }
    />
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 40,
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'white',
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    marginTop: 60,
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  page2: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
});

export default ScanKeluar;
