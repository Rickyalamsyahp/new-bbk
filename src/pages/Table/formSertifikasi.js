/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState, useMemo} from 'react';
import {
  ActivityIndicator,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import DateTimePicker from '@react-native-community/datetimepicker';
import {Button, Gap, Input} from '../../components';
import {Header} from '../../components/molekul';
import moment from 'moment';
import {getData} from '../../utils';
import {SafeAreaView} from 'react-native';

const FormSertifikasi = ({onRequestClose = () => {}, visible, item = null}) => {
  if (item !== null) {
    var finalArray = item.Barang.map(function (obj) {
      return obj.idBarang;
    });
  }

  const [komoditi, setKomoditi] = useState(item ? item.komoditi : '');
  const [nomorStandarProduk, setNomorStandarProduk] = useState(
    item ? item.nomorStandarProduk : '',
  );
  const [merk, setMerk] = useState(item ? item.merk : '');
  const [noBapc, setNoBapc] = useState(item ? item.noBapc : '');
  const [namaPerusahaan, setNamaPerusahaan] = useState(
    item ? item.namaPerusahaan : '',
  );
  const [alamatPabrik, setAlamatPabrik] = useState(
    item ? item.alamatPabrik : '',
  );
  const [namaImportir, setNamaImportir] = useState(
    item ? item.namaImportir : '',
  );
  const [alamatImportir, setAlamatImportir] = useState(
    item ? item.alamatImportir : '',
  );
  const [nomorSuratTugas, setNomorSuratTugas] = useState(
    item ? item.nomorSuratTugas : '',
  );
  const [namaPetugas, setNamaPetugas] = useState(item ? item.namaPetugas : '');
  const [kuantiti, setKuantiti] = useState(item ? item.jumlah.kuantiti : '');
  const [satuan, setSatuan] = useState(item ? item.jumlah.satuan : '');
  const [selectedItems, setSelectedItems] = useState(item ? finalArray : []);
  const [dataBarang, setDataBarang] = useState([]);
  const [show, setShow] = useState(false);
  const [showAwal, setShowAwal] = useState(false);
  const [showAkhir, setShowAkhir] = useState(false);
  const [date, setDate] = useState(
    item ? new Date(item.tanggalSuratTugas) : new Date(),
  );
  const [dateAwal, setDateAwal] = useState(
    item ? new Date(item.tanggalPengambilan.tanggalAwal) : new Date(),
  );
  const [dateAkhir, setDateAkhir] = useState(
    item ? new Date(item.tanggalPengambilan.tanggalAkhir) : new Date(),
  );
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setKomoditi(item ? item.komoditi : '');
    setNomorStandarProduk(item ? item.nomorStandarProduk : '');
    setMerk(item ? item.merk : '');
    setNoBapc(item ? item.noBapc : '');
    setNamaPerusahaan(item ? item.namaPerusahaan : '');
    setAlamatPabrik(item ? item.alamatPabrik : '');
    setNamaImportir(item ? item.namaImportir : '');
    setAlamatImportir(item ? item.alamatImportir : '');
    setNomorSuratTugas(item ? item.nomorSuratTugas : '');
    setNamaPetugas(item ? item.namaPetugas : '');
    setKuantiti(item ? item.jumlah.kuantiti : '');
    setSatuan(item ? item.jumlah.satuan : '');
    setSelectedItems(item ? finalArray : []);
    setDate(item ? new Date(item.tanggalSuratTugas) : new Date());
    setDateAwal(
      item ? new Date(item.tanggalPengambilan.tanggalAwal) : new Date(),
    );
    setDateAkhir(
      item ? new Date(item.tanggalPengambilan.tanggalAkhir) : new Date(),
    );

    getDataBarang();
  }, [item]);

  const getDataBarang = async e => {
    let token = await getData('user');
    try {
      let res = await fetch('https://qrcode.sinovatif.com/service/barang/all', {
        method: 'GET',
        headers: {
          token: token,
        },
      })
        .then(response => response.json())
        .then(data => {
          const response = data;
          const _data = [];
          for (let i = 0; i < response.datas.datas.length; i++) {
            const element = response.datas.datas[i];
            _data.push({
              id: element.idBarang,
              category: element.categoryBarang,
            });
            setDataBarang(_data);
          }
        });
    } catch (error) {
      alert(error);
    }
  };

  const tambahPengajuan = async () => {
    const res = selectedItems.map(data => {
      return {id: data};
    });
    const tanggal = {
      tanggalAwal: moment(dateAwal).format('YYYY-MM-DD'),
      tanggalAkhir: moment(dateAkhir).format('YYYY-MM-DD'),
    };
    const jumlah = {
      kuantiti: kuantiti,
      satuan: satuan,
    };
    const data = {
      komoditi: komoditi,
      nomorStandarProduk: nomorStandarProduk,
      Barang: res,
      jumlah: jumlah,
      merk: merk,
      tanggalPengambilan: tanggal,
      noBapc: noBapc,
      namaPerusahaan: namaPerusahaan,
      alamatPabrik: alamatPabrik,
      namaImportir: namaImportir,
      alamatImportir: alamatImportir,
      nomorSuratTugas: nomorSuratTugas,
      tanggalSuratTugas: moment(date).format('YYYY-MM-DD'),
      namaPetugas: namaPetugas,
    };
    if (item === null) {
      try {
        let token = await getData('user');
        let resp = await fetch(
          `https://qrcode.sinovatif.com/service/pengajuan-sertifikasi/store`,
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              token: token,
            },
            body: JSON.stringify(data),
          },
        )
          .then(response => response.json())
          .then(responseJson => {
            setKomoditi('');
            setNomorStandarProduk('');
            setMerk('');
            setNoBapc('');
            setNamaPerusahaan('');
            setAlamatPabrik('');
            setNamaImportir('');
            setAlamatImportir('');
            setNomorSuratTugas('');
            setNamaPetugas('');
            setKuantiti('');
            setSatuan('');
            setSelectedItems([]);
            setDateAwal(new Date());
            setDateAkhir(new Date());
            setDate(new Date());
            onRequestClose();
            // setPage(1)
            // console.log(data);
            // setInternetCheck(internetCheck + 1);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        let token = await getData('user');
        let resp = await fetch(
          `https://qrcode.sinovatif.com/service/pengajuan-sertifikasi/${item.idPengajuansertifikasi}/update`,
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              token: token,
            },
            body: JSON.stringify(data),
          },
        )
          .then(response => response.json())
          .then(responseJson => {
            console.log(responseJson);
            onRequestClose();
          });
      } catch (error) {
        console.log(error);
        alert(error);
      }
    }
  };

  const cancel = () => {
    if (item) {
      onRequestClose();
    } else {
      setKomoditi('');
      setNomorStandarProduk('');
      setMerk('');
      setNoBapc('');
      setNamaPerusahaan('');
      setAlamatPabrik('');
      setNamaImportir('');
      setAlamatImportir('');
      setNomorSuratTugas('');
      setNamaPetugas('');
      setKuantiti('');
      setSatuan('');
      setDateAwal(new Date());
      setDateAkhir(new Date());
      setSelectedItems([]);
      setDate(moment(new Date()));
      onRequestClose();
    }
    
    
  };

  const onChange = (event, value) => {
    if (Platform.OS === 'android') {
      if (event.type == 'set') {
        //ok button
        setShow(false);
        setDate(value);
      } else {
        //cancel Button
        setShow(false);
        return null;
      }
    }
  };
  const onChangeAwal = (event, value) => {
    if (Platform.OS === 'android') {
      if (event.type == 'set') {
        //ok button
        setShowAwal(false);
        setDateAwal(value);
      } else {
        //cancel Button
        setShowAwal(false);
        return null;
      }
    }
  };

  const onChangeAkhir = (event, value) => {
    if (Platform.OS === 'android') {
      if (event.type == 'set') {
        //ok button
        setShowAkhir(false);
        setDateAkhir(value);
      } else {
        //cancel Button
        setShowAkhir(false);
        return null;
      }
    }
  };

  const showPicker = () => {
    setShow(true);
  };

  const showPickerAwal = () => {
    setShowAwal(true);
  };

  const showPickerAkhir = () => {
    setShowAkhir(true);
  };

  return (
    <View style={styles.centeredView}>
      <Modal
        style={{flex: 1}}
        animationType="fade"
        transparent={false}
        visible={visible}
        onRequestClose={onRequestClose}>
        <ScrollView style={{flex: 1}}>
          <SafeAreaView style={{flex: 1}}>
            <View>
              {item ? (
                <Header title="Edit Pengujian Sertifikasi" type="dark" />
              ) : (
                <Header title="Tambah Pengujian Sertifikasi" type="dark" />
              )}
            </View>
            <View style={{flex: 1, backgroundColor: 'white', padding: 20}}>
              {/* <ActivityIndicator color="white" style={{marginLeft: 6}} /> */}
              <View>
                <Input
                  label="1. Komoditi"
                  value={komoditi}
                  onChangeText={value => setKomoditi(value)}
                />
                <Gap height={14} />
                <Input
                  label="2. Nomor Standar Produk"
                  value={nomorStandarProduk}
                  onChangeText={value => setNomorStandarProduk(value)}
                />
                <Gap height={14} />
                <Text style={{color: '#7D8797', fontSize: 16}}>
                  3. Pilih Barang
                </Text>
                <MultiSelect
                  hideTags
                  items={dataBarang}
                  uniqueKey="id"
                  onSelectedItemsChange={selectedItems => {
                    console.log(selectedItems);
                    setSelectedItems(selectedItems);
                  }}
                  selectedItems={selectedItems}
                  selectText="Pick Items"
                  searchInputPlaceholderText="Search Items..."
                  onChangeInput={text => console.log(text)}
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="category"
                  searchInputStyle={{color: '#CCC'}}
                  submitButtonColor="#48d22b"
                  submitButtonText="Submit"
                />

                <Gap height={14} />
                <Input
                  label="4. Merk"
                  value={merk}
                  onChangeText={value => setMerk(value)}
                />
                <Gap height={14} />
                <Text style={{color: '#7D8797', fontSize: 16}}>5. Jumlah</Text>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Gap height={8} />
                  <TextInput
                    style={{
                      width: 100,
                      borderWidth: 2,
                      borderColor: '#E9E9E9',
                    }}
                    keyboardType="numeric"
                    placeholder={'Kuantiti'}
                    value={kuantiti}
                    onChangeText={value => setKuantiti(value)}
                  />
                  <View style={{marginLeft: 15}}>
                    <TextInput
                      style={{
                        width: 100,
                        borderWidth: 2,
                        borderColor: '#E9E9E9',
                      }}
                      placeholder={'Satuan'}
                      value={satuan}
                      onChangeText={value => setSatuan(value)}
                    />
                  </View>
                </View>
                <Gap height={14} />
                <Text style={{color: '#7D8797', fontSize: 16}}>
                  6. Tanggal Pengambilan Contoh
                </Text>
                {!showAwal && (
                  <View>
                    <Button
                      title="Pilih Tanggal Tanggal Awal"
                      onPress={showPickerAwal}
                    />
                    <Text>
                      Tanggal yang awal yang dipilih:{' '}
                      {moment(dateAwal).format('YYYY-MM-DD')}
                    </Text>
                  </View>
                )}
                <Gap height={20} />
                {!showAkhir && (
                  <View>
                    <Button
                      title="Pilih Tanggal Tanggal Akhir"
                      onPress={showPickerAkhir}
                    />
                    <Text>
                      Tanggal yang akhir yang dipilih:{' '}
                      {moment(dateAkhir).format('YYYY-MM-DD')}
                    </Text>
                  </View>
                )}
                {useMemo(() => {
                  return (
                    showAwal && (
                      <DateTimePicker
                        testID="dateTimePickerAwal"
                        value={dateAwal}
                        mode={'date'}
                        is24Hour={true}
                        display="default"
                        onChange={onChangeAwal}
                      />
                    )
                  );
                }, [showAwal])}

                {useMemo(() => {
                  return (
                    showAkhir && (
                      <DateTimePicker
                        testID="dateTimePickerAkhir"
                        value={dateAkhir}
                        mode={'date'}
                        is24Hour={true}
                        display="default"
                        onChange={onChangeAkhir}
                      />
                    )
                  );
                }, [showAkhir])}

                <Gap height={14} />
                <Input
                  label="7. No BAPC"
                  value={noBapc}
                  onChangeText={value => setNoBapc(value)}
                />
                <Gap height={14} />
                <Input
                  label="8. Nama Perusahaan"
                  value={namaPerusahaan}
                  onChangeText={value => setNamaPerusahaan(value)}
                />
                <Gap height={14} />
                <Text style={{color: '#7D8797', fontSize: 16}}>
                  9. Alamat Pabrik
                </Text>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: '#E9E9E9',
                  }}
                  multiline={true}
                  numberOfLines={5}
                  value={alamatPabrik}
                  onChangeText={value => setAlamatPabrik(value)}
                />
                <Gap height={14} />
                <Input
                  label="10. Nama Importir"
                  value={namaImportir}
                  onChangeText={value => setNamaImportir(value)}
                />
                <Gap height={14} />
                <Text style={{color: '#7D8797', fontSize: 16}}>
                  11. Alamat Importir
                </Text>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: '#E9E9E9',
                  }}
                  multiline={true}
                  numberOfLines={5}
                  value={alamatImportir}
                  onChangeText={value => setAlamatImportir(value)}
                />
                <Gap height={14} />
                <Input
                  label="12. No. Surat Tugas"
                  value={nomorSuratTugas}
                  onChangeText={value => setNomorSuratTugas(value)}
                />
                <Gap height={14} />
                <Text style={{color: '#7D8797', fontSize: 16}}>
                  13. Tanggal Surat Tugas
                </Text>

                {!show && (
                  <View>
                    <Button title="Pilih Tanggal" onPress={showPicker} />
                  </View>
                )}
                {useMemo(() => {
                  return (
                    show && (
                      <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={'date'}
                        is24Hour={true}
                        display="default"
                        onChange={onChange}
                      />
                    )
                  );
                }, [show])}
                <Text>
                  Tanggal yang Telah dipilih:{' '}
                  {moment(date).format('YYYY-MM-DD')}
                </Text>
                <Gap height={14} />
                <Input
                  label="14. Nama Petugas"
                  value={namaPetugas}
                  onChangeText={value => setNamaPetugas(value)}
                />
                <Gap height={40} />
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    margin: 30,
                  }}>
                  {item ? (
                    <TouchableOpacity
                      style={{
                        borderRadius: 4,
                        backgroundColor: '#22B573',
                        height: 40,
                        width: 100,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                      onPress={tambahPengajuan}>
                      <Text style={{color: 'white'}}>Edit</Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={{
                        borderRadius: 4,
                        backgroundColor: '#22B573',
                        height: 40,
                        width: 100,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                      onPress={tambahPengajuan}>
                      <Text style={{color: 'white'}}>Tambah</Text>
                    </TouchableOpacity>
                  )}

                  <TouchableOpacity
                    style={{
                      borderRadius: 4,
                      backgroundColor: 'red',
                      height: 40,
                      width: 100,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={cancel}>
                    <Text style={{color: 'white'}}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SafeAreaView>
        </ScrollView>
      </Modal>
    </View>
  );
};
export default FormSertifikasi;
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
  },
});
