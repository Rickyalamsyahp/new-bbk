/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

export const ListItem = ({item, onPress, onDelete, onGetQrCode, ...props}) => {
  const [expanded, setExpanded] = useState(false);

  const onItemPress = () => {
    onPress();
    setExpanded(!expanded);
  };

  return (
    <TouchableOpacity style={styles.wrap} onPress={onItemPress} key={item.idPengajuansertifikasi}>
      <View style={{flex: 1}} key={item.idPengajuansertifikasi}>
        <View style={styles.container}>
          <View
            style={{
              flex: 1,
              marginLeft: 15,
              marginTop: 5,
            }}>
            <Text style={styles.text}>1. No Bapc : {item.noBapc}</Text>
          </View>
        </View>
        {expanded && (
          <View style={styles.test}>
            <View>
              <Text style={{opacity: 0.7}}>2. Barang: </Text>
            </View>
            {item.Barang.map(r => {
              return (
                <View>
                  <Text style={{opacity: 0.7, marginLeft: 15}}>
                    - {r.categorySample}
                  </Text>
                </View>
              );
            })}
            <Text style={{opacity: 0.7, marginTop: 5}}>
              3. Nomer Standar Produk : {item.nomorStandarProduk}
            </Text>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>4. Merk : {item.merk}</Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                5. Nama Importir : {item.namaImportir}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                6. Alamat Importir : {item.namaImportir}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                7. Nama Perusahaan : {item.namaPerusahaan}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                8. Alamat Importir : {item.namaImportir}
              </Text>
            </View>
            <View style={{marginTop: 30}}>
              <Text>
                Aksi :
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity
                      style={{backgroundColor: '#F93A3A'}}
                      onPress={onDelete}>
                      <Text style={{color: 'white'}}>Delete</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity style={{backgroundColor: '#29ABE2'}} onPress={props.onUpdate}>
                      <Text style={{color: 'white'}}>Edit</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity
                      style={{backgroundColor: 'green'}}
                      onPress={onGetQrCode}>
                      <Text style={{color: 'white'}}>QR Code</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Text>
            </View>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrap: {
    borderColor: '#ccc',
    borderWidth: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {width: 10, height: 10},
    shadowOpacity: 0.2,
  },
  container: {flexDirection: 'row', height: 30},
  text: {opacity: 0.7},
  test: {
    marginLeft: 15,
    justifyContent: 'space-between',
    flex: 1,
  },
});
