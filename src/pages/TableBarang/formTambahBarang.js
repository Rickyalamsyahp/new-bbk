import {add} from 'lodash';
import React, {useEffect, useState} from 'react';
import {
  Image,
  Modal,
  PermissionsAndroid,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as ImagePicker from 'react-native-image-picker';
import {Button, Gap, Input} from '../../components';
import {getData, useForm} from '../../utils';
import {Header} from '../../components/molekul';

const options = {
  title: 'konsepKoding',
  takePhotoButtonTitle: 'Take photo with your camera',
  chooseFromLibraryButtonTitle: 'Choose photo from library',
};
const FormBarang = ({
  navigation,
  visible,
  openCameraWithPermission = () => {},
  item = null,
  onRequestClose,
  internetCheck,
  setInternetCheck,
}) => {
  const [kategoriBarang, setKategoriBarang] = useState(
    item ? item.categoryBarang : '',
  );
  const [kodeProduksiBarang, setKodeProduksiBarang] = useState(
    item ? item.kodeproduksiBarang : '',
  );
  const [thicknessBarang, setThicknessBarang] = useState(
    item ? item.thicknessBarang : '',
  );
  const [sampleSizeBarang, setSampleSizeBarang] = useState(
    item ? item.samplesizeBarang : '',
  );
  const [amountBarang, setAmountBarang] = useState(
    item ? item.amountBarang : '',
  );
  const [responseCamera, setResponseCamera] = useState(null);

  useEffect(() => {
    setKategoriBarang(item ? item.categoryBarang : '');
    setKodeProduksiBarang(item ? item.kodeproduksiBarang : '');
    setThicknessBarang(item ? item.thicknessBarang : '');
    setSampleSizeBarang(item ? item.samplesizeBarang : '');
    setAmountBarang(item ? item.amountBarang : '');
    console.log(item);
  }, [item]);

  const tambah = async () => {
    if (item === null) {
      try {
        let data = {
          kodeproduksiBarang: kodeProduksiBarang,
          categoryBarang: kategoriBarang,
          thicknessBarang: thicknessBarang,
          samplesizeBarang: sampleSizeBarang,
          amountBarang: amountBarang,
          // uri: responseCamera,
        };
        let token = await getData('user');
        await fetch(`https://qrcode.sinovatif.com/service/barang/store`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            token: token,
          },
          body: JSON.stringify(data),
        })
          .then(response => response.json())
          .then(responseJs => {
            console.log(responseJs);
            setAmountBarang('');
            setKodeProduksiBarang('');
            setThicknessBarang('');
            setSampleSizeBarang('');
            setKategoriBarang('');
            setResponseCamera(null);
            setInternetCheck(internetCheck + 1);
            onRequestClose();
            console.log(data);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        let data = {
          kodeproduksiBarang: kodeProduksiBarang,
          categoryBarang: kategoriBarang,
          thicknessBarang: thicknessBarang,
          samplesizeBarang: sampleSizeBarang,
          amountBarang: amountBarang,
          // uri: responseCamera,
        };
        let token = await getData('user');
        await fetch(
          `https://qrcode.sinovatif.com/service/barang/${item.idBarang}/update`,
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              token: token,
            },
            body: JSON.stringify(data),
          },
        )
          .then(response => response.json())
          .then(responseJs => {
            setInternetCheck(internetCheck + 1);
            onRequestClose();
            console.log(data);
          });
      } catch (error) {
        console.log(error);
      }
    }
  };

  const cancel = () => {
    if (item) {
      onRequestClose();
    } else{
      setAmountBarang('');
      setKodeProduksiBarang('');
      setThicknessBarang('');
      setSampleSizeBarang('');
      setKategoriBarang('');
      setResponseCamera(null);
      onRequestClose();
    }
  };

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={false}
        visible={visible}
        onRequestClose={onRequestClose}>
        <ScrollView>
          <View>
            {item ? (
              <Header title="Edit Barang" type="dark" />
            ) : (
              <Header title="Tambah Barang" type="dark" />
            )}
          </View>
          <View style={{flex: 1, backgroundColor: 'white', padding: 20}}>
            <View>
              <Text style={{fontSize: 16}}>Barang</Text>
              <View>
                <TouchableOpacity
                  style={{
                    borderRadius: 4,
                    backgroundColor: '#22B573',
                    height: 40,
                    width: 100,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={
                    (openCameraWithPermission = async () => {
                      try {
                        const granted = await PermissionsAndroid.request(
                          PermissionsAndroid.PERMISSIONS.CAMERA,
                          {
                            title: 'App Camera Permission',
                            message: 'App needs access to your camera ',
                            buttonNeutral: 'Ask Me Later',
                            buttonNegative: 'Cancel',
                            buttonPositive: 'OK',
                          },
                        );
                        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                          ImagePicker.launchCamera(
                            {
                              mediaType: 'photo',
                              includeBase64: false,
                              maxHeight: 200,
                              maxWidth: 200,
                            },
                            response => {
                              const data = response;
                              const _data = [];
                              for (let i = 0; i < data.assets.length; i++) {
                                const element = data.assets[i];
                                _data.push({
                                  fileName: element.fileName,
                                  type: element.type,
                                  uri: element.uri,
                                });
                              }
                              _data.map(d => {
                                const source = d.uri;
                                setResponseCamera(source);
                              });
                            },
                          );
                        } else {
                          console.log('Camera permission denied');
                        }
                      } catch (err) {
                        console.warn(err);
                      }
                    })
                  }>
                  {responseCamera === null ? (
                    <Text style={{color: 'white'}}>Ambil Foto</Text>
                  ) : (
                    <Image style={styles.icon} source={{uri: responseCamera}} />
                  )}
                </TouchableOpacity>
                <Input
                  label="Category   "
                  value={kategoriBarang}
                  onChangeText={value => setKategoriBarang(value)}
                />
                <Gap height={24} />
                <Input
                  label="Kode Produksi"
                  value={kodeProduksiBarang}
                  onChangeText={value => setKodeProduksiBarang(value)}
                />
                <Gap height={24} />
                <Input
                  label="Thickness"
                  value={thicknessBarang}
                  onChangeText={value => setThicknessBarang(value)}
                />
                <Input
                  label="Sample size"
                  value={sampleSizeBarang}
                  onChangeText={value => setSampleSizeBarang(value)}
                />
                <Gap height={24} />
                <Input
                  label="Amount"
                  value={amountBarang}
                  onChangeText={value => setAmountBarang(value)}
                />
              </View>
              <Gap height={20} />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  margin: 30,
                }}>
                {item ? (
                  <TouchableOpacity
                    style={{
                      borderRadius: 4,
                      backgroundColor: '#22B573',
                      height: 40,
                      width: 100,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={tambah}>
                    <Text style={{color: 'white'}}>Edit</Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={{
                      borderRadius: 4,
                      backgroundColor: '#22B573',
                      height: 40,
                      width: 100,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={tambah}>
                    <Text style={{color: 'white'}}>Tambah</Text>
                  </TouchableOpacity>
                )}

                <TouchableOpacity
                  style={{
                    borderRadius: 4,
                    backgroundColor: 'red',
                    height: 40,
                    width: 100,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={cancel}>
                  <Text style={{color: 'white'}}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </Modal>
    </View>
  );
};
export default FormBarang;
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    padding: 10,
  },
  icon: {
    height: 100,
    width: 100,
  },
});
