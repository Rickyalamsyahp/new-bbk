import {Card} from 'native-base';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {ActivityIndicator, RefreshControl} from 'react-native';
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import {Transition, Transitioning} from 'react-native-reanimated';
import {Logo} from '../../assets';
import {HomeProfile} from '../../components';
import {deletData, getData} from '../../utils';
import FormBarang from './formTambahBarang';
// import FormEditBarang from './formTambahBarangEdit';
import {ListItem} from './listItemBarang';
import {Menu, MenuItem, MenuDivider} from 'react-native-material-menu';

const options = {
  title: 'konsepKoding',
  takePhotoButtonTitle: 'Take photo with your camera',
  chooseFromLibraryButtonTitle: 'Choose photo from library',
};

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const TableBarang = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [dataList, setDataList] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [visible, setVisible] = useState(false);
  const [internetCheck, setInternetCheck] = useState(0);
  const [profile, setProfile] = useState({
    namaUser: '',
    namaRole: '',
  });
  const [page, setPage] = useState(1);
  const transitionRef = useRef();
  const [selectedItem, setSelectedItem] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [dataPagination,setDataPagination]= useState({})
  const [fetchingStatus, setFetchingStatus] = useState(false);

  const transition = <Transition.Change interpolation="easeInOut" />;

  const onPress = () => {
    transitionRef.current.animateNextTransition();
  };

  const modal = () => {
    setShowForm(true);
    setSelectedItem(null);
  };

  const Profile = () => {
    navigation.navigate('Profile');
  };

  const hideMenu = () => setVisible(false);
  const showMenu = () => setVisible(true);

  const logout = () => {
    deletData('user');
    deletData('dataUser');
    navigation.replace('GetStarted');
  };

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  useEffect(() => {
    setLoading(true);
    getUserData();
    getDataTable();
    navigation.addListener('focus', () => {
      setLoading(true);
      getUserData();
      getDataTable();
      setPage(1);
    });
  }, []);

  useEffect(() => {
    getDataTable();
  }, [page]);

  const getDataTable = async e => {
    let token = await getData('user');
    console.log(token);
    try {
      let res = await fetch(`https://qrcode.sinovatif.com/service/barang/all?page=${page}`, {
        method: 'GET',
        headers: {
          token: token,
        },
      })
        .then(response => response.json())
        .then(responseJson => {
          setDataList([
            ...dataList,
            ...responseJson.datas.datas,
          ]);
          setDataPagination(responseJson.datas.pagination)
        })
       
        .finally(() => setLoading(false));
      // console.log(dataList)
    } catch (error) {
      alert(error);
    }
  };

  const renderItem = ({item}) => {
    return (
      <ListItem
        item={item}
        onPress={onPress}
        onUpdate={() => {
          setShowForm(true);
          setSelectedItem(item);
        }}
        onDelete={() => {
          Alert.alert('Peringatan', 'Anda yakin akan menghapus data ini ?', [
            {
              text: 'Tidak',
              onPress: () => console.log('Tidak'),
            },
            {
              text: 'Iya',
              onPress: async () => {
                try {
                  let token = await getData('user');
                  const response = await fetch(
                    `https://qrcode.sinovatif.com/service/barang/${item.idBarang}/delete`,
                    {
                      method: 'POST',
                      headers: {
                        token: token,
                      },
                    },
                  );
                  const data = await response.json();
                  setInternetCheck(internetCheck + 1);
                } catch (error) {
                  console.log(error);
                }
              },
            },
          ]);
        }}
      />
    );
  };

  const FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: 'white',
        }}
      />
    );
  };

  const Render_Footer = () => {
    return (
      <View style={styles.footerStyle}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            padding: 7,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#22B573',
            borderRadius: 5,
            display: dataPagination.countRows === dataList.length?'none':"flex"
          }}
          onPress={() => {
            setPage(p => {
              const a = p + 1;
              return a;
            });
          }}>
          <Text style={styles.TouchableOpacity_Inside_Text}>
            Load More Data
          </Text>
          {fetchingStatus ? (
            <ActivityIndicator color="white" style={{marginLeft: 6}} />
          ) : null}
        </TouchableOpacity>
      </View>
    );
  };

  const getUserData = () => {
    getData('dataUser').then(res => {
      setProfile(res);
      console.log(res);
    });
  };
  
  const password = () => {
    navigation.navigate("UpdatePassword")
  }

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 10,
          marginRight: 10,
        }}>
        <Card style={{width: 180, height: 50}}>
          <Logo style={{width: 100, height: 50}} />
        </Card>
        <Card style={{width: 180, height: 50}}>
          <Menu
            visible={visible}
            anchor={
              <Text onPress={showMenu}>
                <HomeProfile
                  profile={profile}
                  style={{width: 100, height: 100}}
                />
              </Text>
            }
            onRequestClose={hideMenu}>
            <MenuItem onPress={Profile}>Profile</MenuItem>
            <MenuItem onPress={password}>Edit Password</MenuItem>
            <MenuDivider />
            <MenuItem onPress={logout}>Logout</MenuItem>
          </Menu>
        </Card>
      </View>
      <View style={{flex: 1, marginTop: 50}}>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            style={{
              backgroundColor: '#22B573',
              paddingVertical: 10,
              borderRadius: 10,
              width: 200,
            }}
            onPress={modal}>
            <Text style={{textAlign: 'center', color: 'white'}}>Add</Text>
          </TouchableOpacity>
        </View>
        {isLoading ? (
          <Text>Loading...</Text>
        ) : (
          <>
            <Transitioning.View
              ref={transitionRef}
              transition={transition}
              style={{flex: 1}}>
              <View style={{flex: 1}}>
                <FlatList
                  data={dataList}
                  refreshing={refreshing}
                  onRefresh={onRefresh}
                  keyExtractor={(item, index) => `${item.id}${index}`}
                  renderItem={renderItem}
                  ItemSeparatorComponent={FlatListItemSeparator}
                  ListFooterComponent={Render_Footer}
                />
              </View>
            </Transitioning.View>
          </>
        )}
        {/* form */}
        <View style={styles.centeredView}>
          <FormBarang
            visible={showForm}
            onRequestClose={() => {
              setShowForm(false);
            }}
            internetCheck={internetCheck}
            setInternetCheck={setInternetCheck}
            item={selectedItem}
            SetopenCameraWithPermission={() => {}}
          />
        </View>
      </View>
    </View>
  );
};

export default TableBarang;
const styles = StyleSheet.create({
  page: {
    marginLeft: 40,
    marginRight: 40,
    flex: 1,
    backgroundColor: 'white',
  },
  page2: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20,
    fontFamily: 'Nunito-SemiBold',
    color: '#112340',
    marginTop: 40,
    marginBottom: 40,
  },
  inputsContainer: {
    flex: 1,
    marginBottom: 20,
  },
  buttonStyle: {
    backgroundColor: '#307ecc',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#307ecc',
    height: 40,
    alignItems: 'center',
    borderRadius: 2,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 15,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
  footerStyle: {
    padding: 7,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 2,
    borderTopColor: 'white',
  },
  TouchableOpacity_Inside_Text: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
  },
});
