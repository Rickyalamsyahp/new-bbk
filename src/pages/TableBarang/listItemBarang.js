/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import moment from 'moment';

export const ListItem = ({item, onPress, onDelete, ...props}) => {
  const [expanded, setExpanded] = useState(false);

  const onItemPress = () => {
    onPress();
    setExpanded(!expanded);
  };

  return (
    <TouchableOpacity style={styles.wrap} onPress={onItemPress}>
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <View
            style={{
              flex: 1,
              // flexDirection: 'row',
              // justifyContent: 'space-around',
              marginTop: 5,
              marginLeft: 7,
            }}>
            <Text style={styles.text}>1. Category : {item.categoryBarang}</Text>
            {/* <View style={{marginLeft: 70}}>
              <Text style={styles.text}>
                {moment(item.createdAt).format('YYYY-MM-DD')}
              </Text>
            </View> */}
          </View>
        </View>
        {expanded && (
          <View style={styles.test}>
            <Text style={styles.text}>
              2. Tickness Barang : {item.thicknessBarang}
            </Text>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                3. Amount Barang : {item.amountBarang}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                4. Kode Produksi Barang : {item.kodeproduksiBarang}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                5. Sample Size Barang : {item.samplesizeBarang}
              </Text>
            </View>
            <View style={{marginTop: 30}}>
              <Text>
                Aksi :
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity
                      style={{backgroundColor: '#F93A3A'}}
                      onPress={onDelete}>
                      <Text style={{color: 'white'}}>Delete</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity
                      style={{backgroundColor: '#29ABE2'}}
                      onPress={props.onUpdate}>
                      <Text style={{color: 'white'}}>Edit</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Text>
            </View>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrap: {
    borderColor: '#ccc',
    borderWidth: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {width: 10, height: 10},
    shadowOpacity: 0.2,
  },
  container: {flexDirection: 'row', height: 30},
  text: {opacity: 0.7},
  test: {
    marginLeft: 8,
    justifyContent: 'space-around',
    flex: 1,
  },
});
