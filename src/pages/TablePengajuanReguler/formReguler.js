/* eslint-disable react-native/no-inline-styles */
import {Picker} from '@react-native-picker/picker';
import React, {useEffect, useState} from 'react';
import {TouchableOpacity} from 'react-native';
import {Modal, ScrollView, StyleSheet, Text, View} from 'react-native';
import {Button, Gap, Input} from '../../components';
import {Header} from '../../components/molekul';
import {getData} from '../../utils';

const FormReguler = ({
  onRequestClose = () => {},
  visible,
  item = null,
  navigation,
}) => {
  const [hasilverifikasiPengujianreguler, setHasilverifikasiPengujianreguler] =
    useState(item ? item.hasilverifikasiPengujianreguler : '');
  const [
    catatanhasilverifikasiPengujianreguler,
    setCatatanhasilverifikasiPengujianreguler,
  ] = useState(item ? item.catatanhasilverifikasiPengujianreguler : '');
  const [noberitaacaraPengajuanreguler, setNoberitaacaraPengajuanreguler] =
    useState(item ? item.noberitaacaraPengajuanreguler : '');
  const [namadilaporanPengujianreguler, setNamadilaporanPengujianreguler] =
    useState(item ? item.namadilaporanPengujianreguler : '');
  const [alamatdilaporanPengujianreguler, setAlamatdilaporanPengujianreguler] =
    useState(item ? item.alamatdilaporanPengujianreguler : '');
  const [namadibiayaPengujianreguler, setNamadibiayaPengujianreguler] =
    useState(item ? item.namadibiayaPengujianreguler : '');
  const [alamatdibiayaPengujianreguler, setAlamatdibiayaPengujianreguler] =
    useState(item ? item.alamatdibiayaPengujianreguler : '');
  const [perkiraanbiayaPengujianreguler, setPerkiraanbiayaPengujianreguler] =
    useState(item ? item.perkiraanbiayaPengujianreguler : '');
  const [uangmukaPengujianreguler, setUangmukaPengujianreguler] = useState(
    item ? item.uangmukaPengujianreguler : '',
  );
  const [statusPengajuanreguler, setStatusPengajuanreguler] = useState(
    item ? item.statusPengajuanreguler : '',
  );
  const [
    petugaspelayananPengajuanreguler,
    setPetugaspelayananPengajuanreguler,
  ] = useState(item ? item.petugaspelayananPengajuanreguler : '');
  const [namapemohonPengajuanreguler, setNamapemohonPengajuanreguler] =
    useState(item ? item.namapemohonPengajuanreguler : '');
  const [alamatpemohonPengajuanreguler, setAlamatpemohonPengajuanreguler] =
    useState(item ? item.alamatpemohonPengajuanreguler : '');
  const [noDatapengajuanreguler, setNoDatapengajuanreguler] = useState(
    item ? item.dataPengajuanreguler.noDatapengajuanreguler : '',
  );
  const [tandacontohDatapengajuanreguler, setTandacontohDatapengajuanreguler] =
    useState(
      item ? item.dataPengajuanreguler.tandacontohDatapengajuanreguler : '',
    );
  const [jumlahDatapengajuanreguler, setJumlahDatapengajuanreguler] = useState(
    item ? item.dataPengajuanreguler.jumlahDatapengajuanreguler : '',
  );
  const [jenisujiDatapengajuanreguler, setJenisujiDatapengajuanreguler] =
    useState(
      item ? item.dataPengajuanreguler.jenisujiDatapengajuanreguler : '',
    );
  const [
    kondisicontohDatapengajuanreguler,
    setKondisicontohDatapengajuanreguler,
  ] = useState(
    item ? item.dataPengajuanreguler.kondisicontohDatapengajuanreguler : '',
  );
  const [tandacontohVerifikasipetugas, setTandacontohVerifikasipetugas] =
    useState(item ? item.verifikasiPetugas.tandacontohVerifikasipetugas : '');
  const [jumlahVerifikasipetugas, setJumlahVerifikasipetugas] = useState(
    item ? item.verifikasiPetugas.jumlahVerifikasipetugas : '',
  );
  const [jenisujisVerifikasipetugas, setJenisujisVerifikasipetugas] = useState(
    item ? item.verifikasiPetugas.jenisujisVerifikasipetugas : '',
  );
  const [kondisicontohVerifikasipetugas, setKondisicontohVerifikasipetugas] =
    useState(item ? item.verifikasiPetugas.kondisicontohVerifikasipetugas : '');
  const [tandacontohVerifikasikasie, setTandacontohVerifikasikasie] = useState(
    item ? item.verifikasiKasie.tandacontohVerifikasikasie : '',
  );
  const [jumlahVerifikasikasie, setJumlahVerifikasikasie] = useState(
    item ? item.verifikasiKasie.jumlahVerifikasikasie : '',
  );
  const [jenisujiVerifikasikasie, setJenisujiVerifikasikasie] = useState(
    item ? item.verifikasiKasie.jenisujiVerifikasikasie : '',
  );
  const [kondisicontohVerifikasikasie, setKondisicontohVerifikasikasie] =
    useState(item ? item.verifikasiKasie.kondisicontohVerifikasikasie : '');
  const [jeniscontohPengajuanreguler, setJeniscontohPengajuanreguler] =
    useState(item ? item.jeniscontohPengajuanreguler : '');
  const [
    keterangancontohPengajuanreguler,
    setKeterangancontohPengajuanreguler,
  ] = useState(item ? item.keterangancontohPengajuanreguler : '');
  const [pengambilcontohPengajuanreguler, setPengambilcontohPengajuanreguler] =
    useState(item ? item.pengambilcontohPengajuanreguler : '');
  const [selectedValue, setSelectedValue] = useState('Indonesia');

  useEffect(() => {
    setHasilverifikasiPengujianreguler(
      item ? item.hasilverifikasiPengujianreguler : '',
    );

    setCatatanhasilverifikasiPengujianreguler(
      item ? item.catatanhasilverifikasiPengujianreguler : '',
    );
    setNoberitaacaraPengajuanreguler(
      item ? item.noberitaacaraPengajuanreguler : '',
    );
    setNamadilaporanPengujianreguler(
      item ? item.namadilaporanPengujianreguler : '',
    );
    setAlamatdilaporanPengujianreguler(
      item ? item.alamatdilaporanPengujianreguler : '',
    );
    setNamadibiayaPengujianreguler(
      item ? item.namadibiayaPengujianreguler : '',
    );
    setAlamatdibiayaPengujianreguler(
      item ? item.alamatdibiayaPengujianreguler : '',
    );
    setPerkiraanbiayaPengujianreguler(
      item ? item.perkiraanbiayaPengujianreguler : '',
    );
    setStatusPengajuanreguler(item ? item.statusPengajuanreguler : '');
    setUangmukaPengujianreguler(item ? item.uangmukaPengujianreguler : '');

    setPetugaspelayananPengajuanreguler(
      item ? item.petugaspelayananPengajuanreguler : '',
    );
    setNamapemohonPengajuanreguler(
      item ? item.namapemohonPengajuanreguler : '',
    );
    setAlamatpemohonPengajuanreguler(
      item ? item.alamatpemohonPengajuanreguler : '',
    );
    setNoDatapengajuanreguler(
      item ? item.dataPengajuanreguler.noDatapengajuanreguler : '',
    );
    setTandacontohDatapengajuanreguler(
      item ? item.dataPengajuanreguler.tandacontohDatapengajuanreguler : '',
    );
    setJumlahDatapengajuanreguler(
      item ? item.dataPengajuanreguler.jumlahDatapengajuanreguler : '',
    );
    setJenisujiDatapengajuanreguler(
      item ? item.dataPengajuanreguler.jenisujiDatapengajuanreguler : '',
    );
    setKondisicontohDatapengajuanreguler(
      item ? item.dataPengajuanreguler.kondisicontohDatapengajuanreguler : '',
    );
    setTandacontohVerifikasipetugas(
      item ? item.verifikasiPetugas.tandacontohVerifikasipetugas : '',
    );
    setJumlahVerifikasipetugas(
      item ? item.verifikasiPetugas.jumlahVerifikasipetugas : '',
    );
    setJenisujisVerifikasipetugas(
      item ? item.verifikasiPetugas.jenisujisVerifikasipetugas : '',
    );
    setKondisicontohVerifikasipetugas(
      item ? item.verifikasiPetugas.kondisicontohVerifikasipetugas : '',
    );
    setTandacontohVerifikasikasie(
      item ? item.verifikasiKasie.tandacontohVerifikasikasie : '',
    );
    setJumlahVerifikasikasie(
      item ? item.verifikasiKasie.jumlahVerifikasikasie : '',
    );
    setJenisujiVerifikasikasie(
      item ? item.verifikasiKasie.jenisujiVerifikasikasie : '',
    );
    setKondisicontohVerifikasikasie(
      item ? item.verifikasiKasie.kondisicontohVerifikasikasie : '',
    );
    setJeniscontohPengajuanreguler(
      item ? item.jeniscontohPengajuanreguler : '',
    );
    setKeterangancontohPengajuanreguler(
      item ? item.keterangancontohPengajuanreguler : '',
    );
    setPengambilcontohPengajuanreguler(
      item ? item.pengambilcontohPengajuanreguler : '',
    );
    console.log(item);
  }, [item]);

  const tambahPengajuan = async () => {
    const all = {
      noDatapengajuanreguler: noDatapengajuanreguler,
      tandacontohDatapengajuanreguler: tandacontohDatapengajuanreguler,
      jumlahDatapengajuanreguler: jumlahDatapengajuanreguler,
      jenisujiDatapengajuanreguler: jenisujiDatapengajuanreguler,
      kondisicontohDatapengajuanreguler: kondisicontohDatapengajuanreguler,
    };
    const verifikasiPetugas = {
      tandacontohVerifikasipetugas: tandacontohVerifikasipetugas,
      jumlahVerifikasipetugas: jumlahVerifikasipetugas,
      jenisujisVerifikasipetugas: jenisujisVerifikasipetugas,
      kondisicontohVerifikasipetugas: kondisicontohVerifikasipetugas,
    };
    const verifikasiKasie = {
      tandacontohVerifikasikasie: tandacontohVerifikasikasie,
      jumlahVerifikasikasie: jumlahVerifikasikasie,
      jenisujiVerifikasikasie: jenisujiVerifikasikasie,
      kondisicontohVerifikasikasie: kondisicontohVerifikasikasie,
    };
    const data = {
      jeniscontohPengajuanreguler: jeniscontohPengajuanreguler,
      keterangancontohPengajuanreguler: keterangancontohPengajuanreguler,
      pengambilcontohPengajuanreguler: pengambilcontohPengajuanreguler,
      hasilverifikasiPengujianreguler: hasilverifikasiPengujianreguler,
      catatanhasilverifikasiPengujianreguler:
        catatanhasilverifikasiPengujianreguler,
      noberitaacaraPengajuanreguler: noberitaacaraPengajuanreguler,
      bahasapenulisanPengajuanreguler: selectedValue,
      namadilaporanPengujianreguler: namadilaporanPengujianreguler,
      alamatdilaporanPengujianreguler: alamatdilaporanPengujianreguler,
      namadibiayaPengujianreguler: namadibiayaPengujianreguler,
      alamatdibiayaPengujianreguler: alamatdibiayaPengujianreguler,
      perkiraanbiayaPengujianreguler: perkiraanbiayaPengujianreguler,
      uangmukaPengujianreguler: uangmukaPengujianreguler,
      petugaspelayananPengajuanreguler: petugaspelayananPengajuanreguler,
      namapemohonPengajuanreguler: namapemohonPengajuanreguler,
      alamatpemohonPengajuanreguler: alamatpemohonPengajuanreguler,
      statusPengajuanreguler: statusPengajuanreguler,
      data: all,
      verifikasiPetugas: verifikasiPetugas,
      verifikasiKasie: verifikasiKasie,
    };
    if (item === null) {
      try {
        let token = await getData('user');
        await fetch(
          `https://qrcode.sinovatif.com/service/pengajuan-reguler/store`,
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              token: token,
            },
            body: JSON.stringify(data),
          },
        )
          .then(response => response.json())
          .then(responseJson => {
            console.log(data);
            onRequestClose();
            setHasilverifikasiPengujianreguler('');
            setCatatanhasilverifikasiPengujianreguler('');
            setNoberitaacaraPengajuanreguler('');
            setNamadilaporanPengujianreguler('');
            setAlamatdilaporanPengujianreguler('');
            setNamadibiayaPengujianreguler('');
            setAlamatdibiayaPengujianreguler('');
            setPerkiraanbiayaPengujianreguler('');
            setUangmukaPengujianreguler('');
            setPetugaspelayananPengajuanreguler('');
            setNamapemohonPengajuanreguler('');
            setAlamatpemohonPengajuanreguler('');
            setNoDatapengajuanreguler('');
            setTandacontohDatapengajuanreguler('');
            setJumlahDatapengajuanreguler('');
            setJenisujiDatapengajuanreguler('');
            setKondisicontohDatapengajuanreguler('');
            setTandacontohVerifikasipetugas('');
            setJumlahVerifikasipetugas('');
            setJenisujisVerifikasipetugas('');
            setKondisicontohVerifikasipetugas('');
            setTandacontohVerifikasikasie('');
            setJumlahVerifikasikasie('');
            setJenisujiVerifikasikasie('');
            setKondisicontohVerifikasikasie('');
            setJeniscontohPengajuanreguler('');
            setKeterangancontohPengajuanreguler('');
            setPengambilcontohPengajuanreguler('');
            setStatusPengajuanreguler('')
            setSelectedValue('Indonesia');
          });
      } catch (error) {}
    } else {
      try {
        let token = await getData('user');
        await fetch(
          `https://qrcode.sinovatif.com/service/pengajuan-reguler/${item.idPengajuanreguler}/update`,
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              token: token,
            },
            body: JSON.stringify(data),
          },
        )
          .then(response => response.json())
          .then(responseJson => {
            console.log(responseJson);
            console.log(data);
            onRequestClose();
          });
      } catch (error) {
        console.log(error);
      }
    }
  };

  const cancel = () => {
    if (item) {
      onRequestClose();
    } else{
      setHasilverifikasiPengujianreguler('');
      setCatatanhasilverifikasiPengujianreguler('');
      setNoberitaacaraPengajuanreguler('');
      setNamadilaporanPengujianreguler('');
      setAlamatdilaporanPengujianreguler('');
      setNamadibiayaPengujianreguler('');
      setAlamatdibiayaPengujianreguler('');
      setPerkiraanbiayaPengujianreguler('');
      setUangmukaPengujianreguler('');
      setPetugaspelayananPengajuanreguler('');
      setNamapemohonPengajuanreguler('');
      setAlamatpemohonPengajuanreguler('');
      setNoDatapengajuanreguler('');
      setTandacontohDatapengajuanreguler('');
      setJumlahDatapengajuanreguler('');
      setJenisujiDatapengajuanreguler('');
      setKondisicontohDatapengajuanreguler('');
      setStatusPengajuanreguler('')
      setTandacontohVerifikasipetugas('');
      setJumlahVerifikasipetugas('');
      setJenisujisVerifikasipetugas('');
      setKondisicontohVerifikasipetugas('');
      setTandacontohVerifikasikasie('');
      setJumlahVerifikasikasie('');
      setJenisujiVerifikasikasie('');
      setKondisicontohVerifikasikasie('');
      setJeniscontohPengajuanreguler('');
      setKeterangancontohPengajuanreguler('');
      setPengambilcontohPengajuanreguler('');
      setSelectedValue('Indonesia');
      onRequestClose();
    }
   
  };

  return (
    <View style={styles.centeredView}>
      <Modal
        style={{flex: 1}}
        animationType="slide"
        transparent={true}
        visible={visible}
        onRequestClose={onRequestClose}>
        <ScrollView>
          <View>
            {item ? (
              <Header title="Edit Pengujian Reguler" type="dark" />
            ) : (
              <Header title="Tambah Pengujian Reguler" type="dark" />
            )}
          </View>
          <View style={{flex: 1, backgroundColor: 'white', padding: 20}}>
            <View style={{flex: 1}}>
              <Input
                label="1. Hasil Verifikasi Pengujian Reguler"
                value={hasilverifikasiPengujianreguler}
                onChangeText={value =>
                  setHasilverifikasiPengujianreguler(value)
                }
              />
              <Input
                label="2. Catatan Hasil Verifikasi Pengujian Reguler"
                value={catatanhasilverifikasiPengujianreguler}
                onChangeText={value =>
                  setCatatanhasilverifikasiPengujianreguler(value)
                }
              />
              <Input
                label="3. No Berita Acara Pengajuan Reguler"
                value={noberitaacaraPengajuanreguler}
                onChangeText={value => setNoberitaacaraPengajuanreguler(value)}
              />
              <Gap height={14} />
              <Text style={{color: '#7D8797', fontSize: 16}}>
                4. Bahasa Penulisan Pengajuan Reguler
              </Text>
              <Picker
                selectedValue={selectedValue}
                style={{height: 50, width: 150, color: '#7D8797'}}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedValue(itemValue)
                }>
                <Picker.Item label="Indonesia" value="indonesia" />
                <Picker.Item label="Inggris" value="inggris" />
              </Picker>
              <Gap height={8} />
              <Input
                label="5. Nama Di Laporan Pengujian Reguler"
                value={namadilaporanPengujianreguler}
                onChangeText={value => setNamadilaporanPengujianreguler(value)}
              />
              <Input
                label="6. Alamat Di Laporan Pengujian Reguler"
                value={alamatdilaporanPengujianreguler}
                onChangeText={value =>
                  setAlamatdilaporanPengujianreguler(value)
                }
              />
              <Input
                label="7. Nama Di Biaya Pengujian Reguler"
                value={namadibiayaPengujianreguler}
                onChangeText={value => setNamadibiayaPengujianreguler(value)}
              />
              <Input
                label="8. Alamat Di Biaya Pengujian Reguler"
                value={alamatdibiayaPengujianreguler}
                onChangeText={value => setAlamatdibiayaPengujianreguler(value)}
              />
              <Input
                label="9. Perkiraan Biaya Pengujian Reguler"
                value={perkiraanbiayaPengujianreguler}
                onChangeText={value => setPerkiraanbiayaPengujianreguler(value)}
              />
              <Input
                label="10. Uang Muka Pengujian Reguler"
                value={uangmukaPengujianreguler}
                keyboardType="numeric"
                onChangeText={value =>
                  setUangmukaPengujianreguler(parseInt(value))
                }
              />
              <Input
                label="11. Petugas Pelayanan Pengajuan Reguler"
                value={petugaspelayananPengajuanreguler}
                onChangeText={value =>
                  setPetugaspelayananPengajuanreguler(value)
                }
              />
              <Input
                label="12. Nama Pemohon Pengajuan Reguler"
                value={namapemohonPengajuanreguler}
                onChangeText={value => setNamapemohonPengajuanreguler(value)}
              />
              <Input
                label="13. Alamat Pemohon Pengajuan Reguler"
                value={alamatpemohonPengajuanreguler}
                onChangeText={value => setAlamatpemohonPengajuanreguler(value)}
              />
              <Input
                label="14. No Data Pengajuan Reguler"
                value={noDatapengajuanreguler}
                onChangeText={value => setNoDatapengajuanreguler(value)}
              />
              <Input
                label="15. Tanda Contoh Data Pengajuan Reguler"
                value={tandacontohDatapengajuanreguler}
                onChangeText={value =>
                  setTandacontohDatapengajuanreguler(value)
                }
              />
              <Input
                label="16. Jumlah Data Pengajuan Reguler"
                value={jumlahDatapengajuanreguler}
                onChangeText={value => setJumlahDatapengajuanreguler(value)}
              />
              <Input
                label="17. Jenis Uji Data Pengajuan Reguler"
                value={jenisujiDatapengajuanreguler}
                onChangeText={value => setJenisujiDatapengajuanreguler(value)}
              />
              <Input
                label="18. Kondisi Contoh Data Pengajuan Reguler"
                value={kondisicontohDatapengajuanreguler}
                onChangeText={value =>
                  setKondisicontohDatapengajuanreguler(value)
                }
              />
              <Input
                label="19. Tanda Contoh Verifikasi Petugas"
                value={tandacontohVerifikasipetugas}
                onChangeText={value => setTandacontohVerifikasipetugas(value)}
              />
              <Input
                label="20. Jumlah Verifikasi Petugas"
                value={jumlahVerifikasipetugas}
                keyboardType="numeric"
                onChangeText={value =>
                  setJumlahVerifikasipetugas(parseInt(value))
                }
              />
              <Input
                label="21. Jenis Uji Verifikasi Petugas"
                value={jenisujisVerifikasipetugas}
                onChangeText={value => setJenisujisVerifikasipetugas(value)}
              />
              <Input
                label="22. Kondisi Contoh Verifikasi Petugas"
                value={kondisicontohVerifikasipetugas}
                onChangeText={value => setKondisicontohVerifikasipetugas(value)}
              />
              <Input
                label="23. Tanda Contoh Verifikasi Kasie"
                value={tandacontohVerifikasikasie}
                onChangeText={value => setTandacontohVerifikasikasie(value)}
              />
              <Input
                label="24. Jumlah Verifikasi Kasie"
                value={jumlahVerifikasikasie}
                keyboardType="numeric"
                onChangeText={value =>
                  setJumlahVerifikasikasie(parseInt(value))
                }
              />
              <Input
                label="25. Jenis Uji Verifikasi Kasie"
                value={jenisujiVerifikasikasie}
                onChangeText={value => setJenisujiVerifikasikasie(value)}
              />
              <Input
                label="26. kondisi Contoh Verifikasi Kasie"
                value={kondisicontohVerifikasikasie}
                onChangeText={value => setKondisicontohVerifikasikasie(value)}
              />
              <Input
                label="27. Jenis Contoh Pengajuan Reguler"
                value={jeniscontohPengajuanreguler}
                onChangeText={value => setJeniscontohPengajuanreguler(value)}
              />
              <Input
                label="28. Keterangan Contoh Pengajuan Reguler"
                value={keterangancontohPengajuanreguler}
                onChangeText={value =>
                  setKeterangancontohPengajuanreguler(value)
                }
              />
              <Input
                label="29. Pengambil Contoh Pengajuan Reguler"
                value={pengambilcontohPengajuanreguler}
                onChangeText={value =>
                  setPengambilcontohPengajuanreguler(value)
                }
              />
              <Input
                label="30. Status Pengajuan Reguler"
                value={statusPengajuanreguler}
                onChangeText={value => setStatusPengajuanreguler(value)}
              />
              <Gap height={40} />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  margin: 30,
                }}>
                {item ? (
                  <TouchableOpacity
                    style={{
                      borderRadius: 4,
                      backgroundColor: '#22B573',
                      height: 40,
                      width: 100,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={tambahPengajuan}>
                    <Text style={{color: 'white'}}>Edit</Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={{
                      borderRadius: 4,
                      backgroundColor: '#22B573',
                      height: 40,
                      width: 100,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={tambahPengajuan}>
                    <Text style={{color: 'white'}}>Tambah</Text>
                  </TouchableOpacity>
                )}

                <TouchableOpacity
                  style={{
                    borderRadius: 4,
                    backgroundColor: 'red',
                    height: 40,
                    width: 100,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={cancel}>
                  <Text style={{color: 'white'}}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </Modal>
    </View>
  );
};
export default FormReguler;
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
  },
});
