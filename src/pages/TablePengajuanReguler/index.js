import {Card} from 'native-base';
import React, {useEffect, useRef, useState} from 'react';
import {
  Alert,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  PermissionsAndroid,
  View,
  ActivityIndicator,
} from 'react-native';
import {Transition, Transitioning} from 'react-native-reanimated';
import {Logo} from '../../assets';
import {HomeProfile} from '../../components';
import {deletData, getData} from '../../utils';
import FormReguler from './formReguler';
import {ListItem} from './listItem';
// import PushNotification from "react-native-push-notification";
import RNFetchBlob from 'rn-fetch-blob';
import {Menu, MenuItem, MenuDivider} from 'react-native-material-menu';

const TablePengajuanReguler = ({navigation}) => {
  const [profile, setProfile] = useState({
    namaUser: '',
    jabatan: '',
  });
  const [isLoading, setLoading] = useState(true);
  const [showForm, setShowForm] = useState(false);
  const [dataPengajuanReguler, setDataPengajuanReguler] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const [query, setQuery] = useState('');
  const [fullData, setFullData] = useState([]);
  const transitionRef = useRef();
  const [page, setPage] = useState(1);
  const [fetchingStatus, setFetchingStatus] = useState(false);
  const isMounted = useRef(false);
  const [dataPagination,setDataPagination]= useState({})
  const [visible, setVisible] = useState(false);
  const transition = <Transition.Change interpolation="easeInOut" />;
  // var RNFS = require('react-native-fs');

  const logout = () => {
    deletData('user');
    deletData('dataUser');
    navigation.replace('GetStarted');
  };

  const onPress = () => {
    transitionRef.current.animateNextTransition();
  };

  const modal = () => {
    setShowForm(true);
    setSelectedItem(null);
  };

  useEffect(() => {
    getUserData();
    navigation.addListener('focus', () => {
      setLoading(true);
      setDataPengajuanReguler([]);
      getUserData();
      getDataTablePengajuan();
      setPage(1);
      console.log('log page effect atas', page);
    });
  }, []);

  useEffect(() => {
    console.log('log page', page);
    getDataTablePengajuan();
  }, [page]);

  const Profile = () => {
    navigation.navigate('Profile');
  };

  const hideMenu = () => setVisible(false);
  const showMenu = () => setVisible(true);

  const getDataTablePengajuan = async () => {
    let token = await getData('user');
    try {
      await fetch(
        `https://qrcode.sinovatif.com/service/pengajuan-reguler?page=${page}`,
        {
          method: 'GET',
          headers: {
            token: token,
          },
        },
      )
        //Sending the currect offset with get request
        .then(response => response.json())
        .then(responseJson => {
          //Increasing the offset for the next API call
          setDataPengajuanReguler([
            ...dataPengajuanReguler,
            ...responseJson.datas.datas,
          ]);
          setFullData([...dataPengajuanReguler, ...responseJson.datas.datas]);
          setDataPagination(responseJson.datas.pagination)
          setLoading(false);
        });
    } catch (error) {
      console.log(error);
    }
    //Service to get the data from the server to render
  };
  const FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: 'white',
        }}
      />
    );
  };

  const password = () => {
    navigation.navigate("UpdatePassword")
  }

  const Render_Footer = () => {
    return (
      <View style={styles.footerStyle}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            padding: 7,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#22B573',
            borderRadius: 5,
            display: dataPagination.countRows === dataPengajuanReguler.length?'none':dataPengajuanReguler.length<=3?'none':"flex"
          }}
          onPress={() => {
            setPage(p => {
              return p + 1;
            });
          }}>
          <Text style={styles.TouchableOpacity_Inside_Text}>
            Load More Data
          </Text>
          {fetchingStatus ? (
            <ActivityIndicator color="white" style={{marginLeft: 6}} />
          ) : null}
        </TouchableOpacity>
      </View>
    );
  };

  const checkPermission = async item => {

    if (Platform.OS === 'ios') {
      downloadImage(item);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'App needs access to your storage to download Photos',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          console.log('Storage Permission Granted.');
          downloadImage(item);
        } else {
          // If permission denied then show alert
          alert('Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };

  const downloadImage = item => {
    let image_URL = `https://qrcode.sinovatif.com/service/pengajuan-reguler/${item.idPengajuanreguler}/getqrcode`;
    const {fs} = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;
    RNFetchBlob.config({
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        path: `${PictureDir}/${item.idPengajuanreguler} QRCODE.jpeg`,
        description: 'download image',
        mime: 'image/jpeg',
      },
    })
      .fetch('GET', image_URL)
      .then(res => {
        // Showing alert after successful downloading
        // console.log(res);
        alert('Image Downloaded Successfully.');
      });
  };

  const renderItem = ({item}) => {
    return (
      <ListItem
        item={item}
        onPress={onPress}
        onUpdate={() => {
          setShowForm(true);
          setSelectedItem(item);
        }}
        onDelete={() => {
          Alert.alert('Peringatan', 'Anda yakin akan menghapus data ini ?', [
            {
              text: 'Tidak',
              onPress: () => console.log('Tidak'),
            },
            {
              text: 'Iya',
              onPress: async () => {
                try {
                  let token = await getData('user');
                  const response = await fetch(
                    `https://qrcode.sinovatif.com/service/pengajuan-reguler/${item.idPengajuanreguler}/delete`,
                    {
                      method: 'DELETE',
                      headers: {
                        token: token,
                      },
                    },
                  );
                  const data = await response.json();
                  Alert.alert('Data Berhasil dihapus');
                  navigation.push('MainApp');
                } catch (error) {
                  console.log(error);
                }
              },
            },
          ]);
        }}
        onGetQrCode={() => {
          checkPermission(item);
        }}
      />
    );
  };

  const getUserData = () => {
    getData('dataUser').then(res => {
      setProfile(res);
    });
  };
  const handleSearch = text => {
    if (text) {
      const newData = fullData.filter(item => {
        const itemData = item.noberitaacaraPengajuanreguler
          ? item.noberitaacaraPengajuanreguler.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setDataPengajuanReguler(newData);
      setQuery(text);
    } else {
      setDataPengajuanReguler(fullData);
      setQuery(text);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 10,
          marginRight: 10,
        }}>
        <Card style={{width: 180, height: 50}}>
          <Logo style={{width: 100, height: 50}} />
        </Card>
        <Card style={{width: 180, height: 50}}>
          <Menu
            visible={visible}
            anchor={
              <Text onPress={showMenu}>
                <HomeProfile
                  profile={profile}
                  style={{width: 100, height: 100}}
                />
              </Text>
            }
            onRequestClose={hideMenu}>
            <MenuItem onPress={Profile}>Profile</MenuItem>
            <MenuItem onPress={password}>Edit Password</MenuItem>
            <MenuDivider />
            <MenuItem onPress={logout}>Logout</MenuItem>
          </Menu>
        </Card>
      </View>
      <View
        style={{
          backgroundColor: '#fff',
          marginTop: 20,
          flex: 1,
          maxHeight: 50,
          alignItems: 'center',
        }}>
        <View style={{borderWidth: 3, borderColor: '#7D8797', width: 180}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            clearButtonMode="always"
            value={query}
            onChangeText={queryText => handleSearch(queryText)}
            placeholder="Search"
            style={{backgroundColor: '#fff'}}
          />
        </View>
      </View>

      {/* <Button title="logout" onPress={logout} /> */}
      <View style={{alignItems: 'center', marginTop: 5}}>
        <TouchableOpacity
          style={{
            backgroundColor: '#22B573',
            paddingVertical: 10,
            borderRadius: 10,
            width: 200,
          }}
          onPress={modal}>
          <Text style={{textAlign: 'center', color: 'white'}}>Add</Text>
        </TouchableOpacity>
      </View>

      {isLoading ? (
        <ActivityIndicator size="large" />
      ) : (
        <>
          <Transitioning.View
            ref={transitionRef}
            transition={transition}
            style={{flex: 1}}>
            <View style={{flex: 1, marginTop: 20}}>
              <FlatList
                data={dataPengajuanReguler}
                keyExtractor={(item, index) => {
                  return `${item.id}${index}`;
                }}
                renderItem={renderItem}
                ItemSeparatorComponent={FlatListItemSeparator}
                ListFooterComponent={Render_Footer}
              />
            </View>
          </Transitioning.View>
        </>
      )}
      {/* form */}
      <View style={styles.centeredView}>
        <FormReguler
          visible={showForm}
          onRequestClose={() => {
            setShowForm(false);
          }}
          navigation={() => {
            navigation.push('MainApp');
          }}
          item={selectedItem}
        />
      </View>
    </View>
  );
};

export default TablePengajuanReguler;
const styles = StyleSheet.create({
  page: {
    marginLeft: 40,
    marginRight: 40,
    flex: 1,
    backgroundColor: 'white',
  },
  page2: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20,
    fontFamily: 'Nunito-SemiBold',
    color: '#112340',
    marginTop: 40,
    marginBottom: 40,
  },
  inputsContainer: {
    flex: 1,
    marginBottom: 20,
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerStyle: {
    padding: 7,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 2,
    borderTopColor: 'white',
  },
  TouchableOpacity_Inside_Text: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
  },
});
