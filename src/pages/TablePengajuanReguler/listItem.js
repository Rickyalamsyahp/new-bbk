/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

export const ListItem = ({item, onUpdate,onPress, onDelete, onGetQrCode}) => {
  const [expanded, setExpanded] = useState(false);

  const onItemPress = () => {
    onPress();
    setExpanded(!expanded);
  };

  return (
    <TouchableOpacity style={styles.wrap} onPress={onItemPress}>
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <View
            style={{
              flex: 1,
              marginLeft: 15,
              marginTop: 8,
            }}>
            <Text style={styles.text}>
              1. No Berita Acara : {item.noberitaacaraPengajuanreguler}
            </Text>
          </View>
        </View>
        {expanded && (
          <View style={styles.test}>
            <Text style={{opacity: 0.7, marginTop: 5}}>
              2. Hasil Verifikasi Pengujian :{' '}
              {item.hasilverifikasiPengujianreguler}
            </Text>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                3. Catatan Hasil Verifikasi Pengujian :{' '}
                {item.catatanhasilverifikasiPengujianreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                4. Bahasa Pengulisan Reguler :{' '}
                {item.bahasapenulisanPengajuanreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                5. Nama Penguji : {item.namadilaporanPengujianreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                6. Alamat Penguji : {item.alamatdilaporanPengujianreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                7. Nama Biaya Pengujian : {item.namadibiayaPengujianreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                8. Alamat Biaya Pengujian : {item.alamatdibiayaPengujianreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                9. Perkiraan Biaya Pengujian :{' '}
                {item.perkiraanbiayaPengujianreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                10. Uang Muka Pengujian : {item.uangmukaPengujianreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                11. Petugas Pelayanan Pengujian :{' '}
                {item.petugaspelayananPengajuanreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                12. Nama Pemohon Pengujian : {item.namapemohonPengajuanreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                13. Alamat Pemohon Pengujian :{' '}
                {item.alamatpemohonPengajuanreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                14. Alamat Pemohon Pengujian :{' '}
                {item.alamatpemohonPengajuanreguler}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={styles.text}>
                15. Status Pengajuan : {item.statusPengajuanreguler}
              </Text>
            </View>
            <View style={{marginTop: 30}}>
              <Text>
                Aksi :
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity
                      style={{backgroundColor: '#F93A3A'}}
                      onPress={onDelete}>
                      <Text style={{color: 'white'}}>Delete</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity style={{backgroundColor: '#29ABE2'}} onPress={onUpdate}>
                      <Text style={{color: 'white'}}>Edit</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginLeft: 20}}>
                    <TouchableOpacity
                      style={{backgroundColor: 'green'}}
                      onPress={onGetQrCode}>
                      <Text style={{color: 'white'}}>QR Code</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Text>
            </View>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrap: {
    borderColor: '#ccc',
    borderWidth: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {width: 10, height: 10},
    shadowOpacity: 0.2,
  },
  container: {flexDirection: 'row', height: 30},
  text: {opacity: 0.7},
  test: {
    marginLeft: 15,
    justifyContent: 'space-between',
    flex: 1,
  },
});
