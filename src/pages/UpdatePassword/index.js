import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Button, Gap, Header, Input} from '../../components';
import {colors, deletData, getData, storeData, useForm} from '../../utils';

const Password = ({navigation}) => {
  const [form, setForm] = useForm({
    password: '',
    passwordBaru: '',
    konfirmasipasswordBaru: '',
  });
  const [profile, setProfile] = useState({});;

  useEffect(() => {
    getUserData();
  }, []);

  const update = async () => {
    const data = {
      passwordLama: form.password,
      passwordBaru: form.passwordBaru,
      passwordBaruKonfirmasi: form.konfirmasipasswordBaru,
    };

    let token = await getData('user');
    try {
      let res = await fetch(
        `https://qrcode.sinovatif.com/service/akun/${profile.idUser}/update`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            token: token,
          },
          body: JSON.stringify(data),
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          const a = responseJson;
          console.log(a);
          if (a.status === true) {
            alert(a.message);
          } else {
            console.log(a.message);
            alert(a.message);
          }
        });
    } catch (error) {}
    console.log(data, profile.idUser);
  };

  const getUserData = () => {
    getData('dataUser').then(res => {
      setProfile(res);
      console.log(res);
    });
  };

  return (
    <View style={styles.page}>
      <Header title="Edit Profile" onPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.content}>
          <Input
            label="Password Lama"
            secureTextEntry
            value={form.password}
            onChangeText={value => setForm('password', value)}
          />
          <Input
            label="Password Baru"
            secureTextEntry
            value={form.passwordBaru}
            onChangeText={value => setForm('passwordBaru', value)}
          />
          <Input
            label="Konfirmasi Password Baru"
            secureTextEntry
            value={form.KonfirmasipasswordBaru}
            onChangeText={value => setForm('konfirmasipasswordBaru', value)}
          />
          <Gap height={40} />
          <Button title="Save Password" onPress={update} />
        </View>
      </ScrollView>
    </View>
  );
};

export default Password;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  content: {padding: 40, paddingTop: 0},
});
