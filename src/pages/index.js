import Splash from './Splash';
import GetStarted from './GetStarted';
import Register from './Register';
import Login from './Login';
import ScanMasuk from './ScanMasuk';
import ScanKeluar from './ScanKeluar';
import Table from './Table';
import TableBarang from './TableBarang';
import TablePengajuanReguler from './TablePengajuanReguler';
import Profile from './Profile';
import UpdatePassword from './UpdatePassword';
import HistoryReguler from './History'
import HistorySertifikasi from './HistrorySertifikasi'

export {
  Splash,
  GetStarted,
  Register,
  Login,
  ScanMasuk,
  ScanKeluar,
  Table,
  TableBarang,
  TablePengajuanReguler,
  Profile,
  UpdatePassword,
  HistoryReguler,
  HistorySertifikasi
};
