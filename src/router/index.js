import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  GetStarted,
  Login,
  Register,
  Splash,
  ScanMasuk,
  ScanKeluar,
  Table,
  TablePengajuanReguler,
  Profile,
  TableBarang,
  UpdatePassword,
  HistoryReguler,
  HistorySertifikasi
} from '../pages';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {BottomNavigator} from '../components';
// import TableBarang from '../pages/TableBarang';
import {getData} from '../utils';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  const [profile, setProfile] = useState({
    namaUser: '',
    jabatan: '',
  });

  useEffect(() => {
    getUserData();
  }, []);

  const getUserData = () => {
    getData('dataUser').then(res => {
      setProfile(res);
      console.log(res);
    });
  };

  return (
    <>
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
      <Tab.Screen name="Barang" component={TableBarang} />
      {profile.namaRole === 'Layanan' ? (
        <Tab.Screen name="Reguler" component={TablePengajuanReguler} />
      ) : null}
      <Tab.Screen name="Sertifikasi" component={Table}/>
      <Tab.Screen name="ScanMasuk" component={ScanMasuk} />
      <Tab.Screen name="ScanKeluar" component={ScanKeluar} />
    </Tab.Navigator>
    </>
  );
};

const Router = () => {
  return (
    <>
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="Profile"
        component={Profile}
        // options={{headerShown: false}}
      />
      <Stack.Screen
        name="UpdatePassword"
        component={UpdatePassword}
        // options={{headerShown: false}}
      />
      <Stack.Screen
        name="History Reguler"
        component={HistoryReguler}
        // options={{headerShown: false}}
      />
      <Stack.Screen
        name="History Sertifikasi"
        component={HistorySertifikasi}
        // options={{headerShown: false}}
      />
      <Stack.Screen
        name="GetStarted"
        component={GetStarted}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
    </>
  );
};

export default Router;
